class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :genre
      t.string :category
      t.string :code, unique: true
      t.string :email, unique: true
      t.string :password
      t.string :password_confirmation
      
      t.index :code
      t.timestamps
    end
  end
end

class User < ApplicationRecord
  require 'securerandom'

  before_save { self.email = email.downcase }
  before_save :generate_code

  validates :name, presence: true, length: { maximum: 50 }
  validates :genre, presence: true
  validates :category, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true


  private

    def generate_code
      self.code = SecureRandom.urlsafe_base64(5)
    end

end
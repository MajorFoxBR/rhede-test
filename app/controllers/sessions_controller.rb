class SessionsController < ApplicationController

  def index
  end

  def new
  end

  def create
    unless current_user
      @user = User.find_by_email(params[:email])
      if @user && @user.password.eql?(params[:password])
        session[:user_id] = @user.id
        flash[:success] = "You have successfully logged in!"
        redirect_to '/'
      else
        flash[:error] = "Login or Password are incorrect!"
        redirect_to '/login'
      end
    else
      flash[:success] = "User already logged in!"
      redirect_to '/'
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:success] = "You have successfully logged out!"
    redirect_to '/login'
  end

end
class UsersController < ApplicationController
  before_action :authorize, only: [:show, :edit, :update]

  def show
    @user = User.find(params[:id])
  end

  def search
    if params[:code].present?
      @user = User.find_by(code: params[:code])
      if @user
        flash[:success] = "Soldier found!"
        render 'search'
      else
        flash[:error] = "Soldier not found!"
        render 'search'
      end
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Successfully enlisted!"
      redirect_to '/login'
    else
      flash[:error] = "Error while enlisting, try again!"
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if edit_user_params[:password_confirmation].eql?(edit_user_params[:password])
      if @user.update_attributes(edit_user_params)
        flash[:success] = "Successfully edited enlistment!"
        redirect_to @user
      else
        render 'edit'
      end
    else
      flash[:error] = "The password confirmation is not correct."
      render 'edit'
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                    :code, :genre, :category,
                                    :password_confirmation)
    end
    def edit_user_params
      params.require(:user).permit(:name, :password,
                                    :genre, :category,
                                    :password_confirmation)
    end
end
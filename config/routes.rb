Rails.application.routes.draw do
    
  root to: 'welcome#index'

  get '/panel' => 'sessions#index'
  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'
  
  get '/signup' => 'users#new'

  get '/search' => 'users#search'
  post '/search' => 'users#search'

  resources :users

end
